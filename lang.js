/*
EXAMPLE

The current heap would look something like:

0000000000000000000000000000000000000000

And our code might look something like:

...
!01%00#72916F7C5E7A-!08>00.

RR RR 
02 00 01 01 02 00 03 72 91 6F 7C 5E 7A 00 01 08 FF 01

Or in a more concise form:

RR VVVVVVVVVVVVVVVVVVVVVVVVVVVVV
01 00 72 91 6F 7C 5E 7A 00 FF 00

"RR" refers to the register area.

Now at runtime we specify a "boot" address, which in this situation will be 0x08, as this is our first opcode.
The runtime will now start running through our heap. It might look like this

0x08    "FF"    The opcode FF means we're outputting the value at an address from the register.
0x09    "00"    This is the expected register address. Our runtime is now set to output all values the ptr points to.
0x02    "72"    72 is outputted as a char
0x03    "91"    91 is outputted as a char
....    ....    ....
0x07    "00"    00 isn't outputted, but closes the stdout stream. The runtime now returns to the last point.
0x0A    "01"    As no stdout stream is open, this opcode terminates the program.

OPCODE CHEAT SHEET

01      change ptr
02      create register address
03      insert
04      IF address value equals 
FF      output
AF      terminate program

*/

const out = msg => console.log(msg)

function to_hex(val) {
    if (val < 16) {
        return "0" + val.toString(16).toUpperCase()
    } else {
        return val.toString(16).toUpperCase()
    }
}

function compile(code, regsize, heapsize) {
    code = code.replace(/-/g, "00")
               .replace(/\s+/g, "");

    out (code);

    let heap = "0".repeat((regsize+heapsize)*2).split("")

    let get_ptr = (p,mod=0) => regsize*2+p*2+mod;

    let print_heap = h => {
        let upperline = ("RR".repeat(regsize)+"VV".repeat( (heapsize < 32) ? heapsize : 32 )).match(/.{1,2}/g).join(" ");
        let dump = heap.join("").match(/.{1,2}/g)

        for (let i = 1; i < ~~(dump.length/(32+regsize) )+1; i++) {
            dump[i * (32+regsize)] = "\n" + dump[i * (32+regsize)];
        }



        return upperline + "\n" + dump.join(" ");
    }
    let ptr = 0;


    let i = 0;

    let get_2b = () => code[++i]+code[++i];
    let insert = (p,v) => {

        let k = 0;
        for (t = ptr; t < ptr+p+1; t++) {
            heap[get_ptr(ptr,k)] = v[k];
            k++;
        }
        ptr += 1; 
    }
    
    let insert_register = (p,v) => {
        let k = 0;
        for (t = p; t < p+2; t++) {
            heap[p*2+k] = v[k]
            k++;
        }
    }

    out("Register size: "+ to_hex(regsize))

    insert_register(0,to_hex(regsize));

    let put = false;

    while (i < code.length) {

        let cur = i;

        if (put) {
            bytes = get_2b()
            insert(1, bytes);

            if (bytes === "00") {
                put = false;
            }
        } else {

            switch(code[i]) {
                

                case "!":
                    insert(1, "01"); // insert 01 at ptr position [... 01 00 ...]
                    insert(1, get_2b()); // insert the next two bytes (00-FF)  [... 01 ** ...]
                    console.log("SET_PTR", ptr)
                    break

                case "%":
                    insert(1, "02")
                    insert(1, get_2b());
                    console.log("CREATE_REGISTER", ptr)
                    break

                case "#":
                    console.log("INSERT", ptr);
                    insert(1, "03")
                    put = true;
                    break

                case "&":
                    console.log("IF", ptr);
                    insert(1, "04")
                    insert(1, get_2b())
                    insert(1, get_2b())
                    insert(1, get_2b())
                    break
                
                case ">":
                    console.log("STDOUT", ptr);
                    insert(1, "FF");
                    insert(1, get_2b())
                    break

                case "+":
                    console.log("TERMINATE", ptr);
                    insert(1, "AF");
                    break;
            }
        }
        if (!put) i++;
    }


    out(print_heap(heap))
    return heap;

}

code = `
#   48 64 6C 6F 2C 20 77 6F 72 6C 64 21-
%   00
>   01
+
`

f = compile(code, 8, 122); // Compile code onto 64*2 bytes of space, which will be used by the runtime.

function load(heap, start_ptr, size) {

    let get_bytes = pos => heap[pos]+heap[pos+1];
    let to_num    = hex => parseInt(hex, 16);
    
    // read register 0x00 for regsize
    let regsize = to_num(get_bytes(0));
    
    // ptr init
    let get_ptr = (p,mod=0) => regsize*2+p*2+mod;
    let ptr = start_ptr;

    // insertion
    let insert = (p,v) => {
        let k = 0;
        for (t = ptr; t < ptr+p+1; t++) {
            heap[get_ptr(ptr,k)] = v[k];
            k++;
        }
        ptr += 1; 
    }

    // scan for free space
    let mem_entry;

    for (i = 0; i < size; i++) {
        if (get_bytes(get_ptr(i)) == "AF") {
            mem_entry = i+1;
        }
    }

    // begin runtime
    while (get_bytes(get_ptr(ptr)) !== "AF") {
        let cur = ptr;
        out(get_bytes(get_ptr(ptr)))
        ptr++;
    }


}

load(f, 0, 122);